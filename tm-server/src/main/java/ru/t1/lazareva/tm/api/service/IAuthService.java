package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.model.Session;
import ru.t1.lazareva.tm.model.User;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    User check(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    Session validateToken(@Nullable String token) throws Exception;

    void invalidate(@Nullable final Session session) throws Exception;

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email) throws Exception;

}