package ru.t1.lazareva.tm.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.lazareva.tm.api.endpoint.IDomainEndpoint;
import ru.t1.lazareva.tm.api.endpoint.IUserEndpoint;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.dto.request.*;
import ru.t1.lazareva.tm.dto.response.UserLoginResponse;
import ru.t1.lazareva.tm.exception.AbstractException;
import ru.t1.lazareva.tm.marker.SoapCategory;
import ru.t1.lazareva.tm.service.PropertyService;

import java.security.NoSuchAlgorithmException;

@Category(SoapCategory.class)
public class DomainEndpointTest {

    @Nullable
    public final static String ADMIN_LOGIN = "admin";
    @Nullable
    public final static String ADMIN_PASSWORD = "admin";
    @Nullable
    public final static String USER_LOGIN = "test1";
    @Nullable
    public final static String USER_PASSWORD = "test1";
    @Nullable
    public final static String USER_EMAIL = "USER_EMAIL";
    @NotNull
    private final static IPropertyService PROPERTY_SERVICE = new PropertyService();
    @NotNull
    private final static IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);
    @NotNull
    private final static IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);
    @NotNull
    private final static IDomainEndpoint ENDPOINT = IDomainEndpoint.newInstance(PROPERTY_SERVICE);
    @Nullable
    private static String ADMIN_TOKEN;
    @Nullable
    private static String USER_TOKEN;

    @BeforeClass
    public static void before() throws AbstractException, NoSuchAlgorithmException, JsonProcessingException {
        @NotNull final UserLoginRequest loginAdminRequest = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final UserLoginResponse loginAdminResponse = AUTH_ENDPOINT.login(loginAdminRequest);
        ADMIN_TOKEN = loginAdminResponse.getToken();

        @NotNull final UserRegistryRequest request = new UserRegistryRequest(USER_LOGIN, USER_PASSWORD, USER_EMAIL);
        USER_ENDPOINT.registryUser(request);

        @NotNull final UserLoginRequest loginUserRequest = new UserLoginRequest(USER_LOGIN, USER_PASSWORD);
        @NotNull final UserLoginResponse loginUserResponse = AUTH_ENDPOINT.login(loginUserRequest);
        USER_TOKEN = loginUserResponse.getToken();
    }

    @AfterClass
    public static void after() throws AbstractException {
        ENDPOINT.saveDataBackup(new DataBackupSaveRequest(ADMIN_TOKEN));
        ENDPOINT.loadDataBackup(new DataBackupLoadRequest(ADMIN_TOKEN));

        @NotNull final UserLogoutRequest logoutUserRequest = new UserLogoutRequest(USER_TOKEN);
        AUTH_ENDPOINT.logout(logoutUserRequest);
        USER_TOKEN = null;

        @NotNull final UserRemoveRequest removeUserRequest = new UserRemoveRequest(ADMIN_TOKEN, USER_LOGIN);
        USER_ENDPOINT.removeUser(removeUserRequest);

        @NotNull final UserLogoutRequest logoutAdminRequest = new UserLogoutRequest(ADMIN_TOKEN);
        AUTH_ENDPOINT.logout(logoutAdminRequest);
        ADMIN_TOKEN = null;
    }

    @Test
    public void loadDataBackup() throws AbstractException {
        ENDPOINT.saveDataBackup(new DataBackupSaveRequest(ADMIN_TOKEN));
        Assert.assertNotNull(ENDPOINT.loadDataBackup(new DataBackupLoadRequest(ADMIN_TOKEN)));
    }

    @Test
    public void loadDataBase64() throws AbstractException {
        ENDPOINT.saveDataBase64(new DataBase64SaveRequest(ADMIN_TOKEN));
        Assert.assertNotNull(ENDPOINT.loadDataBase64(new DataBase64LoadRequest(ADMIN_TOKEN)));
    }

    @Test
    public void loadDataBinary() throws AbstractException {
        ENDPOINT.saveDataBinary(new DataBinarySaveRequest(ADMIN_TOKEN));
        Assert.assertNotNull(ENDPOINT.loadDataBinary(new DataBinaryLoadRequest(ADMIN_TOKEN)));
    }

    @Test
    public void loadDataJsonFasterXml() throws AbstractException {
        ENDPOINT.saveDataJsonFasterXml(new DataJsonSaveFasterXmlRequest(ADMIN_TOKEN));
        Assert.assertNotNull(ENDPOINT.loadDataJsonFasterXml(new DataJsonLoadFasterXmlRequest(ADMIN_TOKEN)));
    }

    @Test
    public void loadDataJsonJaxB() throws AbstractException {
        ENDPOINT.saveDataJsonJaxB(new DataJsonSaveJaxBRequest(ADMIN_TOKEN));
        Assert.assertNotNull(ENDPOINT.loadDataJsonJaxB(new DataJsonLoadJaxBRequest(ADMIN_TOKEN)));
    }

    @Test
    public void loadDataXmlFasterXml() throws AbstractException {
        ENDPOINT.saveDataXmlFasterXml(new DataXmlSaveFasterXmlRequest(ADMIN_TOKEN));
        Assert.assertNotNull(ENDPOINT.loadDataXmlFasterXml(new DataXmlLoadFasterXmlRequest(ADMIN_TOKEN)));
    }

    @Test
    public void loadDataXmlJaxB() throws AbstractException {
        ENDPOINT.saveDataXmlJaxB(new DataXmlSaveJaxBRequest(ADMIN_TOKEN));
        Assert.assertNotNull(ENDPOINT.loadDataXmlJaxB(new DataXmlLoadJaxBRequest(ADMIN_TOKEN)));
    }

    @Test
    public void loadDataYamlFasterXml() throws AbstractException {
        ENDPOINT.saveDataYamlFasterXml(new DataYamlSaveFasterXmlRequest(ADMIN_TOKEN));
        Assert.assertNotNull(ENDPOINT.loadDataYamlFasterXml(new DataYamlLoadFasterXmlRequest(ADMIN_TOKEN)));
    }

    @Test
    public void saveDataBackup() throws AbstractException {
        Assert.assertNotNull(ENDPOINT.saveDataBackup(new DataBackupSaveRequest(ADMIN_TOKEN)));
    }

    @Test
    public void saveDataBase64() throws AbstractException {
        Assert.assertNotNull(ENDPOINT.saveDataBase64(new DataBase64SaveRequest(ADMIN_TOKEN)));
    }

    @Test
    public void saveDataBinary() throws AbstractException {
        Assert.assertNotNull(ENDPOINT.saveDataBinary(new DataBinarySaveRequest(ADMIN_TOKEN)));
    }

    @Test
    public void saveDataJsonFasterXml() throws AbstractException {
        Assert.assertNotNull(ENDPOINT.saveDataJsonFasterXml(new DataJsonSaveFasterXmlRequest(ADMIN_TOKEN)));
    }

    @Test
    public void saveDataJsonJaxB() throws AbstractException {
        Assert.assertNotNull(ENDPOINT.saveDataJsonJaxB(new DataJsonSaveJaxBRequest(ADMIN_TOKEN)));
    }

    @Test
    public void saveDataXmlFasterXml() throws AbstractException {
        Assert.assertNotNull(ENDPOINT.saveDataXmlFasterXml(new DataXmlSaveFasterXmlRequest(ADMIN_TOKEN)));
    }

    @Test
    public void saveDataXmlJaxB() throws AbstractException {
        Assert.assertNotNull(ENDPOINT.saveDataXmlJaxB(new DataXmlSaveJaxBRequest(ADMIN_TOKEN)));
    }

    @Test
    public void saveDataYamlFasterXml() throws AbstractException {
        Assert.assertNotNull(ENDPOINT.saveDataYamlFasterXml(new DataYamlSaveFasterXmlRequest(ADMIN_TOKEN)));
    }

}